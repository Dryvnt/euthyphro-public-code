#ifndef GRAPH_CANON_REFINE_SUBTREE_HPP
#define GRAPH_CANON_REFINE_SUBTREE_HPP

#include <graph_canon/visitor/visitor.hpp>

#include <boost/graph/properties.hpp>

#include <algorithm>
#include <cassert>
#include <optional>
#include <tuple>
#include <unordered_map>
#include <utility>
#include <vector>

namespace graph_canon {

static unsigned long long SUBTREE_NUM_FAIL = 0;

struct refine_subtree : null_visitor {
    static const std::size_t split_type_cell = 2;
    static const std::size_t aut_tag_subtree_swap = 102;
    static const std::size_t aut_tag_subtree_cycle = 103;

    template <typename Vertex, typename SizeType>
    struct FlatTree {
        struct LevelInfo {
            SizeType cell_begin, cell_end;
            SizeType p_cell_begin, p_cell_end;

            // at which index in FlatTree.vertices
            // this level's vertices starts
            SizeType vertices_idx;
            SizeType clusters_idx;

            SizeType cluster_size = 0;
            SizeType num_clusters = 0;

            LevelInfo(SizeType cell_begin, SizeType cell_end,
                      SizeType p_cell_begin, SizeType p_cell_end,
                      SizeType vertices_idx, SizeType clusters_idx)
                : cell_begin(cell_begin), cell_end(cell_end),
                  p_cell_begin(p_cell_begin), p_cell_end(p_cell_end),
                  vertices_idx(vertices_idx), clusters_idx(clusters_idx) {}

            std::tuple<SizeType, SizeType> clusters_range() {
                SizeType start = clusters_idx;
                SizeType end = start + num_clusters;
                return std::make_pair(start, end);
            }
        };

        // Depends on info from LevelInfo to index properly
        //
        // Note: LevelInfo.cluster_size is 0 until we find first cluster
        // so we can only index w/ cluster_idx = 0 until then.
        // This is okay because that's the first cluster, but be careful.

        struct ClusterInfo {
            Vertex p;
            SizeType vertices_idx;
            SizeType size; // for assert, remove later?

            ClusterInfo(Vertex p, SizeType vertices_idx, SizeType size)
                : p(p), vertices_idx(vertices_idx), size(size) {}

            std::tuple<SizeType, SizeType> vertices_range() {
                SizeType start = vertices_idx;
                SizeType end = start + size;
                return std::make_pair(start, end);
            }
        };

        std::vector<LevelInfo> levels;
        std::vector<Vertex> vertices;
        std::vector<ClusterInfo> clusters;
        std::vector<std::optional<ClusterInfo>> parent_to_cluster_map;

        // We must be done adding clusters from last level before we add level
        void add_level(SizeType cell_begin, SizeType cell_end,
                       SizeType p_cell_begin, SizeType p_cell_end) {
            SizeType v_offset = 0;
            SizeType c_offset = 0;
            if (levels.size() > 0) {
                LevelInfo& last_level = levels.back();
                v_offset = last_level.vertices_idx
                           + last_level.num_clusters * last_level.cluster_size;
                c_offset = last_level.clusters_idx + last_level.num_clusters;
            }
            levels.emplace_back(cell_begin, cell_end, p_cell_begin, p_cell_end,
                                v_offset, c_offset);
        }

        // Adds to last level
        template <typename IndexMap>
        void add_cluster(std::vector<Vertex>& cluster, Vertex p,
                         IndexMap& idx) {
            LevelInfo& last_level = levels.back();

            if (last_level.num_clusters == 0) {
                last_level.cluster_size = cluster.size();
            } else {
                assert(last_level.cluster_size == cluster.size());
                assert(last_level.cluster_size != 0);
            }

            auto v_offset = last_level.vertices_idx
                            + last_level.num_clusters * last_level.cluster_size;
            clusters.emplace_back(p, v_offset, cluster.size());
            last_level.num_clusters += 1;
            parent_to_cluster_map[idx(p)] = clusters.back();
            vertices.insert(std::end(vertices), std::begin(cluster),
                            std::end(cluster));
        }

        FlatTree(SizeType n) : parent_to_cluster_map(n, std::nullopt) {
            levels.reserve(n);
            vertices.reserve(n);
            clusters.reserve(n);
        }
    };

    template <typename State, typename Vertex, typename Partition,
              typename Graph, typename SizeType>
    bool gather_levels(State& state, Partition& pi, Graph& g,
                       SizeType cell_begin, SizeType cell_end,
                       SizeType p_cell_begin, SizeType p_cell_end,
                       FlatTree<Vertex, SizeType>& tree_out) {
        const auto& idx = state.idx;
        std::optional<SizeType> parent_parent_cell = std::nullopt;
        auto cell_size = pi.get_cell_size(cell_begin);
        auto parent_cell_size = pi.get_cell_size(p_cell_begin);

        // Invariant: Tree gets smaller as we go up
        // example: mol_0398
        if (parent_cell_size > cell_size) {
            return false;
        }

        tree_out.add_level(cell_begin, cell_end, p_cell_begin, p_cell_end);

        std::vector<Vertex> children;
        children.reserve(state.n);
        for (SizeType p = p_cell_begin; p < p_cell_end; p++) {
            auto p_v = vertex(pi.get(p), g);
            children.clear();

            const auto es = out_edges(p_v, g);
            for (auto iter_e = es.first; iter_e != es.second; iter_e++) {
                auto c_v = target(*iter_e, g);
                auto c_cell = pi.get_cell_from_v_idx(idx[c_v]);
                if (c_cell == p_cell_begin) {
                    // Parent cell has internal connection:
                    // This is a graph, not a tree!
                    return false;
                }
                if (c_cell == cell_begin) {
                    children.push_back(c_v);
                } else if (parent_cell_size > 1) {
                    // If parent cell is not root cell
                    if (!parent_parent_cell.has_value()) {
                        // Then remember parent of parent
                        parent_parent_cell = c_cell;
                    } else if (c_cell != *parent_parent_cell) {
                        // If multiple candidates exist, we cannot
                        // deduce a subtree
                        return false;
                    }
                }
            }

            tree_out.add_cluster(children, p_v, idx);
        }

        if (!parent_parent_cell.has_value()) {
            // Parent cell is a viable root
            if (parent_cell_size == 1) {
                return true;
            }
            // Parent cell not viable root (example: mol_0111.gjson)
            return false;
        }

        auto parent_parent_end = pi.get_cell_end(*parent_parent_cell);
        return gather_levels(state, pi, g, p_cell_begin, p_cell_end,
                             *parent_parent_cell, parent_parent_end, tree_out);
    }

    template <typename State, typename Aut, typename Vertex, typename SizeType>
    void tree_perm_group_put(State& state, Aut& aut,
                             FlatTree<Vertex, SizeType>& tree, Vertex a,
                             Vertex b) {
        perm_group::put(aut, a, b);
        const auto& idx = state.idx;
        const auto& l = tree.parent_to_cluster_map;

        auto a_opt = l[idx[a]];
        auto b_opt = l[idx[b]];

        // end recursion
        if (!a_opt.has_value()) {
            assert(!b_opt.has_value());
            return;
        }

        auto& a_cluster = a_opt.value();
        auto& b_cluster = b_opt.value();

        assert(a_cluster.p == a);
        assert(b_cluster.p == b);
        assert(a_cluster.size == b_cluster.size);

        for (std::size_t i = 0; i < a_cluster.size; i++) {
            auto sub_a = tree.vertices[a_cluster.vertices_idx + i];
            auto sub_b = tree.vertices[b_cluster.vertices_idx + i];

            tree_perm_group_put(state, aut, tree, sub_a, sub_b);
        }
    }

    template <typename SizeType>
    void reset_aut(std::vector<SizeType>& aut) {
        for (std::size_t i = 0; i < perm_group::degree(aut); i++) {
            perm_group::put(aut, i, i);
        }
    }

    template <typename State, typename Node, typename Vertex, typename SizeType>
    void report_automorphisms(State& state, Node& node,
                              FlatTree<Vertex, SizeType>& tree) {
        const auto& idx = state.idx;

        // change type to map<Vertex, "pair of indices into levels">?
        // right now we're copying clusters?
        // or is this just very memory unsafe
        std::vector<SizeType> aut(state.n);

        for (auto& level : tree.levels) {
            assert(level.cluster_size > 0);

            auto c_idx = level.clusters_idx;
            for (auto c = 0; c < level.num_clusters; c++) {
                auto& cluster = tree.clusters[c_idx + c];
                const auto [v_begin, v_end] = cluster.vertices_range();
                if (level.cluster_size > 1) {
                    reset_aut(aut);
                    Vertex first = tree.vertices[v_begin];
                    Vertex second = tree.vertices[v_begin + 1];

                    tree_perm_group_put(state, aut, tree, first, second);
                    tree_perm_group_put(state, aut, tree, second, first);
                    state.visitor.automorphism_implicit(state, node, aut,
                                                        aut_tag_subtree_swap);
                }
                if (level.cluster_size > 2) {
                    //reset_aut(aut); not needed, but helps mental model
                    Vertex first = tree.vertices[v_begin];
                    Vertex prev = first;
                    for (SizeType i = v_begin + 1; i < v_end; i++) {
                        Vertex current = tree.vertices[i];
                        tree_perm_group_put(state, aut, tree, prev, current);
                        prev = current;
                    }
                    // prev is now last, close the cycle
                    tree_perm_group_put(state, aut, tree, prev, first);
                    state.visitor.automorphism_implicit(state, node, aut,
                                                        aut_tag_subtree_cycle);
                }
            }
        }
    }

    template <typename State, typename Partition, typename Vertex,
              typename SizeType>
    void sort_levels_by_parent(State& state, Partition& pi,
                               FlatTree<Vertex, SizeType>& tree) {
        const auto& idx = state.idx;
        // important to iterate in reverse
        // we must go top-to-bottom in the tree
        for (auto it = tree.levels.rbegin(); it != tree.levels.rend(); it++) {
            const auto cell_begin = it->cell_begin;
            const auto cell_end = it->cell_end;

            const auto [c_begin, c_end] = it->clusters_range();

            // sort clusters, required for soundness
            std::sort(tree.clusters.begin() + c_begin,
                      tree.clusters.begin() + c_end,
                      [pi, idx](const auto a, const auto b) {
                          auto a_parent_pi_pos = pi.get_inverse(idx[a.p]);
                          auto b_parent_pi_pos = pi.get_inverse(idx[b.p]);
                          return a_parent_pi_pos < b_parent_pi_pos;
                      });
            // write_out clusters
            // TODO: answer: cluster internal order doesn't matter?
            // assume yes

            auto write_head = cell_begin;
            for (auto c_idx = c_begin; c_idx < c_end; c_idx++) {
                auto& c = tree.clusters[c_idx];
                const auto [v_begin, v_end] = c.vertices_range();
                for (auto read_head = v_begin; read_head < v_end; read_head++) {
                    Vertex v = tree.vertices[read_head];

                    pi.put_element_on_index(idx[v], write_head);
                    write_head++;
                }
            }
            assert(write_head == cell_end);
        }
    }

    template <typename State, typename Node, typename Partition,
              typename Vertex, typename SizeType>
    bool split_cells(State& state, Node& node, Partition& pi,
                     FlatTree<Vertex, SizeType>& tree) {
        sort_levels_by_parent(state, pi, tree);

        // split each level
        for (auto& level : tree.levels) {
            const SizeType cell_begin = level.cell_begin;
            const SizeType cell_end = level.cell_end;
            state.visitor.refine_cell_split_begin(state, node, state.n,
                                                  cell_begin, cell_end);
            { // block for raii
                auto raii_splitter = pi.split_cell(cell_begin);
                for (SizeType c = cell_begin + 1; c < cell_end; c++) {
                    raii_splitter.add_split(c);
                }
            }
            for (std::size_t c = cell_begin + 1; c < cell_end; c++) {
                bool success = state.visitor.refine_new_cell(state, node, c,
                                                             split_type_cell);
                if (!success) {
                    return false;
                }
            }
            state.visitor.refine_cell_split_end(state, node, state.n,
                                                cell_begin, cell_end);
        }
        return true;
    }

    template <typename State, typename TreeNode>
    RefinementResult refine(State& state, TreeNode& node) {
        using SizeType = typename State::SizeType;
        using Graph = typename State::Graph;
        using Vertex = typename State::Vertex;
        using Edge = typename State::Edge;
        using Partition = typename State::Partition;

        const auto n = state.n;
        const Graph& g = state.g;
        auto& pi = node.pi;
        const auto& idx = state.idx;

        RefinementResult result = RefinementResult::Never;

        SizeType cell_end = 0;
        for (SizeType cell_begin = 0; cell_begin != n; cell_begin = cell_end) {
            cell_end = pi.get_cell_end(cell_begin);
            Vertex v_first = vertex(pi.get(cell_begin), g);

            const std::size_t d = degree(v_first, g);
            if (d != 1) {
                continue; // only degree 1
            }

            const SizeType cell_size = pi.get_cell_size(cell_begin);
            if (cell_size <= 1) {
                continue; // only non-trivial cells
            }

            auto p = target(*out_edges(v_first, g).first, g);
            auto p_cell_begin = pi.get_cell_from_v_idx(idx[p]);
            auto p_cell_end = pi.get_cell_end(idx[p_cell_begin]);

            // special case, clique of 2 vertices not connected to others
            if (p_cell_begin == cell_size) {
                continue;
            }

            FlatTree<Vertex, SizeType> tree(state.n);

            bool res = gather_levels(state, pi, g, cell_begin, cell_end,
                                     p_cell_begin, p_cell_end, tree);

            if (res) {
                report_automorphisms(state, node, tree);
                bool success = split_cells(state, node, pi, tree);
                if (!success) {
                    return RefinementResult::Abort;
                }
                result = RefinementResult::Unchanged;
            } else {
                SUBTREE_NUM_FAIL += 1;
            }
        }
        return result;
    }

    template <typename State>
    void initialize(State& state) {
        SUBTREE_NUM_FAIL = 0;
    }
};
} // namespace graph_canon

#endif //GRAPH_CANON_REFINE_SUBTREE_HPP
