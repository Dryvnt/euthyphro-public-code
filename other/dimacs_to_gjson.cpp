#include "util.hpp"

#include <graph_canon/dimacs_graph_io.hpp>
#include <graph_canon/gjson_graph_io.hpp>
#include <graph_canon/util.hpp>

#include <boost/graph/adjacency_list.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/program_options.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/random_device.hpp>
#include <boost/random/uniform_int_distribution.hpp>

#include <fstream>
#include <functional>
#include <iostream>

namespace po = boost::program_options;

struct Options {
    std::string file;
};

// rst: .. cpp:namespace:: graph_canon
// rst:
// rst: ``dimacs-to-gjson``
// rst: ####################################################
// rst:
// rst: .. program:: dimacs-to-gjson
// rst:
// rst: A program for converting dimacs to gjson format
// rst:

int main(int argc, char** argv) {
    Options options;
    po::options_description optionDesc("Options");
    optionDesc.add_options()
        // rst: .. option:: -h, --help
        // rst:
        // rst:     Print help message.
        ("help,h", "Print help message.")
        // rst: .. option:: -f <file>, --file <file>
        // rst:
        // rst:     Filename with graph to read. Use ``-`` to read form stdin.
        ("file,f", po::value<std::string>(&options.file)->required(),
         "Filename with graph to read. Use '-' to read from stdin.");

    po::variables_map rawOptions;
    try {
        po::store(po::command_line_parser(argc, argv)
                      .options(optionDesc)
                      .allow_unregistered()
                      .run(),
                  rawOptions);
    } catch (const po::error& e) {
        std::cout << e.what() << '\n';
        std::exit(1);
    }
    if (rawOptions.count("help")) {
        std::cout << "Load a graph in DIMACS format.\n";
        std::cout << optionDesc << "\n";
        return 0;
    }
    po::notify(rawOptions);

    using Graph = boost::adjacency_list<
        boost::vecS, boost::vecS, boost::undirectedS,
        boost::property<boost::vertex_name_t, std::size_t>,
        boost::property<boost::edge_name_t, std::size_t>>;
    using VertexDescriptor = boost::graph_traits<Graph>::vertex_descriptor;
    using EdgeDescriptor = boost::graph_traits<Graph>::edge_descriptor;
    const auto parHandler = [](unsigned int src, unsigned int tar) {
        return true;
    };
    const auto loopHandler = [](unsigned int v) { return true; };

    Graph g;
    bool res;
    if (options.file == "-") {
        res = graph_canon::gjson::read(std::cin, g, std::cerr, parHandler,
                                       loopHandler);
    } else {
        std::ifstream ifs(options.file);
        if (!ifs) {
            std::cerr << "Could not open file '" << options.file << "'.\n";
            std::exit(1);
        }
        res = graph_canon::read_dimacs_graph(ifs, g, std::cerr, parHandler,
                                             loopHandler);
    }
    if (!res) {
        std::cerr << "Could not parse input.\n";
        std::exit(1);
    }

    auto vert_func = [](auto& g, auto v) {
        return get(boost::vertex_name_t(), g, v);
    };
    auto edge_func = [](auto& g, auto e) {
        return get(boost::edge_name_t(), g, e);
    };

    graph_canon::gjson::write(std::cout, g, vert_func, edge_func,
                              graph_canon::gjson::identity_map<Graph>);
    return 0;
}
