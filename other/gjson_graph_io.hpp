#ifndef GRAPH_CANON_READ_GJSON_GRAPH_HPP
#define GRAPH_CANON_READ_GJSON_GRAPH_HPP

#include <boost/format.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/properties.hpp>

#include <cassert>
#include <cstdio>
#include <functional>
#include <optional>
#include <string>
#include <tuple>
#include <vector>

#include <iostream>

#include <nlohmann/json.hpp>

namespace graph_canon {

// TODO: Better doc
namespace gjson {

template <typename Graph>
typename boost::graph_traits<Graph>::vertex_descriptor
identity_map(typename boost::graph_traits<Graph>::vertex_descriptor v) {
    return v;
}

template <typename Graph, typename VertexLabelFunc, typename EdgeLabelFunc,
          typename VertexMapFunc>
void write(std::ostream& s, Graph& g, VertexLabelFunc vertex_label_func,
           EdgeLabelFunc edge_label_func, VertexMapFunc vertex_map_func) {
    using namespace nlohmann;

    const auto vs = vertices(g);

    std::vector<json> vert_list;
    std::vector<json> edge_list;

    for (auto iter_v = vs.first; iter_v != vs.second; ++iter_v) {
        const auto label = vertex_label_func(g, *iter_v);

        json v;
        v["label"] = label;

        vert_list.push_back(v);

        const auto es = out_edges(*iter_v, g);
        for (auto iter_e = es.first; iter_e != es.second; ++iter_e) {
            const auto label = edge_label_func(g, *iter_e);

            int src = source(*iter_e, g);
            int tar = target(*iter_e, g);

            src = vertex_map_func(src);
            tar = vertex_map_func(tar);

            // Don't duplicate edges
            if (src <= tar) {
                json e;
                e["source"] = src;
                e["target"] = tar;
                e["label"] = label;

                edge_list.push_back(e);
            }
        }
    }

    json j;
    j["vertices"] = vert_list;
    j["edges"] = edge_list;

    s << j.dump();
    s << std::endl;
}

// TODO: Better doc
// TODO: exception handling for every json call
// TODO: assert -> proper erroring
// TODO: allow user to define how to store labels (callbacks? :: Graph Descriptor -> void, modifies graph?)
// TODO: move this into graph_canon::gjson

template <typename Graph, typename ParallelHandler, typename LoopHandler>
bool read(std::istream& s, Graph& graph, std::ostream& err,
          ParallelHandler parHandler, LoopHandler loopHandler) {
    using namespace nlohmann;
    using Vertex = typename boost::graph_traits<Graph>::vertex_descriptor;
    using VertexLabel =
        typename boost::property_traits<typename boost::property_map<
            Graph, boost::vertex_name_t>::type>::value_type;
    using EdgeLabel =
        typename boost::property_traits<typename boost::property_map<
            Graph, boost::edge_name_t>::type>::value_type;
    json j;
    try {
        j = json::parse(s);
    } catch (json::parse_error& wah) {
        err << "Graph JSON file is not valid, parseable JSON" << std::endl;
        return false;
    }

    const std::vector<json> verts_json = j["vertices"];
    const std::vector<json> edges_json = j["edges"];
    const unsigned int n_verts = verts_json.size();
    const unsigned int n_edges = edges_json.size();

    std::vector<Vertex> vertices;

    for (const auto vj : verts_json) {
        const VertexLabel label = vj["label"];
        Vertex v = add_vertex(graph);

        put(boost::vertex_name_t(), graph, v, label);
        vertices.push_back(v);
    }

    for (const auto e : edges_json) {
        const unsigned int src = e["source"];
        const unsigned int tar = e["target"];
        const EdgeLabel label = e["label"];

        if (src > n_verts) {
            err << "Invalid source index" << src << "in edge (" << src << ","
                << tar << ").";
            return false;
        }
        if (tar > n_verts) {
            err << "Invalid target index" << tar << "in edge (" << src << ","
                << tar << ").";
            return false;
        }
        if (src == tar) {
            if (!loopHandler(src))
                continue;
        }

        const auto ep = edge(vertices[src], vertices[tar], graph);
        if (ep.second) {
            if (!parHandler(src, tar))
                continue;
        }

        add_edge(vertices[src], vertices[tar], label, graph);
    }

    return true;
}

} // namespace gjson

} // namespace graph_canon

#endif /* GRAPH_CANON_READ_GJSON_GRAPH_HPP */
