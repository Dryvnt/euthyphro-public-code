#!/usr/bin/env python3

import argparse
import random

parser = argparse.ArgumentParser(
    description='Generate a fasta protein string of size N')
parser.add_argument('N', type=int,
                    help='Length of random protein')
parser.add_argument('-s', '--seed', type=int,
                    help='Length of random protein')
args = parser.parse_args()

if(args.seed is not None):
    random.seed(args.seed)

acids = ['A', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K',
         'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'W', 'Y']

choices = random.choices(acids, k=args.N)

print(''.join(choices))
