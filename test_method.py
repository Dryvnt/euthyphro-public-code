#!/usr/bin/env python3

import subprocess
import os
import sys

protein_file = 'Q6CJ57.fasta'
data_folder = './notes/test_data'
csv_folder = os.path.expanduser('~/Documents/thesis/data')


def run_test(exec_name, base, added, strip=False, collapse=False, permute=False):
    exec_loc = './build/' + exec_name
    log_basename = f'{data_folder}/{base}-{added}-{exec_name}'
    if strip:
        log_basename += '+strip'
    if collapse:
        log_basename += '+collapse'
    if permute:
        log_basename += '+permute'
    command = ['./hyperfinetest.py', exec_loc, protein_file, str(base), str(added), '--json', f'{log_basename}.json']
    if strip:
        command.append('--strip')
    if collapse:
        command.append('--collapse')
    if permute:
        command.append('--permute')
    print(' '.join(command))
    subprocess.run(command, check=True)


def scaletest(exec_name, nmin, nmax, nstep, seed=0xDEADBEEF, strip=False, collapse=False):
    exec_loc = './build/' + exec_name
    log_basename = f'{data_folder}/scale/{exec_name}-{nmin}-{nmax}-{nstep}'
    command = ['./faketest.py',  '--seed', str(seed), exec_loc,
               str(nmin), str(nmax), str(nstep)]
    if strip:
        command.append('--strip')
        log_basename += '+strip'
    if collapse:
        command.append('--collapse')
        log_basename += '+collapse'
    log_basename += '.csv'
    print(' '.join(command))
    if not os.path.exists(os.path.dirname(log_basename)):
        os.makedirs(os.path.dirname(log_basename))
    with open(log_basename, 'w') as outfile:
        subprocess.run(command, stdout=outfile, check=True)


#run_test('euthyphro-dfs-f-none-none-none-none-none', 250, 25)
#run_test('euthyphro-dfs-f-none-degree_1-none-none-none', 250, 25)
#run_test('euthyphro-dfs-f-none-subtree-none-none-none', 250, 25)
#run_test('euthyphro-dfs-f-none-subtree-none-none-none', 650, 65)
#run_test('euthyphro-dfs-f-size_2-subtree-none-none-none', 650, 65)
#run_test('euthyphro-dfs-f-size_2-subtree-none-none-none', 650, 65, permute=True)
#run_test('euthyphro-dfs-fl-size_2-subtree-none-none-none', 650, 65)
#run_test('euthyphro-dfs-fl-size_2-subtree-none-none-none', 650, 65, permute=True)
#run_test('euthyphro-dfs-f-size_2-subtree-none-none-none', 650, 65, strip=True)
#run_test('euthyphro-dfs-f-size_2-subtree-none-none-none', 650, 65, collapse=True)
#run_test('euthyphro-dfs-f-size_2-subtree-none-none-none', 1774, 180, strip=True)
run_test('euthyphro-dfs-f-size_2-subtree-none-none-none', 1774, 180, strip=True, permute=True)

#scaletest('euthyphro-dfs-f-size_2-subtree-none-none-none', 200, 15000, 100, strip=True)
#scaletest('euthyphro-dfs-f-size_2-subtree-none-none-none', 200, 15000, 100, collapse=True)
#scaletest('euthyphro-dfs-f-size_2-subtree-none-none-none', 200, 15000, 100, strip=True, collapse=True)

subprocess.run(['./process_data_folder.sh', data_folder, csv_folder], check=True)
