#!/usr/bin/env python3

import argparse
import difflib
import subprocess
import sys
import json

from relabel import relabel
from permute import permute

parser = argparse.ArgumentParser(
    description='Test euthyphro on permutations of a given GJSON string')
parser.add_argument('-f', '--file', type=str, default='-',
                    help='File to read GJSON from. Set to - or leave undefined for stdin.')
parser.add_argument('-v', '--vlabels', type=int, default=0,
                    help='Number of labels to use for vertices. Will not relabel if 0.')
parser.add_argument('-e', '--elabels', type=int, default=0,
                    help='Number of labels to use for edges. Will not relabel if 0.')
parser.add_argument('-n', '--niters', type=int, default=1,
                    help='Number of times to apply test procedure. Must be at least 1.')
parser.add_argument('-t', '--timeout', type=int, default=None,
                    help='Timeout for test')
args = parser.parse_args()

if args.niters < 1:
    sys.exit('NITERS must be at least 1.')

obj = None
if(args.file == '-'):
    obj = json.loads(sys.stdin.read())
else:
    with open(args.file, 'r') as fp:
        obj = json.loads(fp.read())


def get_canon_str(obj):
    obj_str = json.dumps(obj)
    run = subprocess.run(['./build/euthyphro', '-f', '-'],
                         input=obj_str, encoding=sys.getdefaultencoding(),
                         check=True,
                         timeout=args.timeout,
                         stdout=subprocess.PIPE)
    return run.stdout


relabel(obj, args.vlabels, args.elabels)
permute(obj)

canon_str = get_canon_str(obj)

for i in range(0, args.niters):
    print(f'Run {i}...', end='')
    permute(obj)
    test_str = get_canon_str(obj)
    if canon_str == test_str:
        print('pass')
    else:
        print('fail')
        sys.exit(canon_str + 'was not equal to\n' + test_str)
