#!/usr/bin/env python3

import subprocess
import argparse
import sys
import os

parser = argparse.ArgumentParser(
    description='Run a test suite of all amino acids using ./perform_test.py')
parser.add_argument('e', type=str,
                    help='Executable to use')
parser.add_argument('f', type=str,
                    help='File to read FASTA from')
parser.add_argument('n', type=int,
                    help='Number of characters to read from f')
parser.add_argument('a', type=int,
                    help='Number of each acid to append')
parser.add_argument('--strip', action='store_true',
                    help='Strip hydrogens')
parser.add_argument('--collapse', action='store_true',
                    help='Collapse graph')
parser.add_argument('--permute', action='store_true',
                    help='Permute graph before piping into executable')
parser.add_argument('--md', type=str, help='File to output md table to')
parser.add_argument('--csv', type=str, help='File to output csv table to')
parser.add_argument('--json', type=str, help='File to output json data to')
args = parser.parse_args()

acids = ['', 'A', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K',
         'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'W', 'Y']

commands = [f'./perform_test.py {args.e} {args.f} {args.n} ' + f'{a}' * args.a for a in acids]


def insert_immediately(command, addition):
    s = command.split()
    s.insert(1, addition)
    return ' '.join(s)


if args.strip:
    commands = list(
        map(lambda s: insert_immediately(s, '--strip'), commands))
if args.collapse:
    commands = list(
        map(lambda s: insert_immediately(s, '--collapse'), commands))
if args.permute:
    commands = list(
        map(lambda s: insert_immediately(s, '--permute'), commands))

# this is definitely not a hack, no sir
commands[0] += ' '


def ensure_dir_exists(path):
    dirname = os.path.dirname(path)
    if not os.path.exists(dirname):
        os.makedirs(dirname)


args_markdown = []
if args.md is not None:
    ensure_dir_exists(args.md)
    args_markdown = ['--export-markdown', args.md]
args_csv = []
if args.csv is not None:
    ensure_dir_exists(args.csv)
    args_markdown = ['--export-csv', args.csv]
args_json = []
if args.json is not None:
    ensure_dir_exists(args.json)
    args_markdown = ['--export-json', args.json]

args_meta = ['--runs', '10', '--warmup', '1']

subprocess.run(['hyperfine'] + args_meta + args_markdown + args_csv + commands, check=True)
