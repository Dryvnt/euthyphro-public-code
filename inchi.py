#!/usr/bin/env python3

# run inchi-1 on a given sdf file
# example usage: inchi.py mol.sdf
# example usage: cat mol.sdf | inchi.py

import argparse
import subprocess
import tempfile
import os
import sys

parser = argparse.ArgumentParser(
    description='Wrapper script for inchi-1')
parser.add_argument('sdf', nargs='?', type=argparse.FileType(
    'r'), default=sys.stdin, help='File to read SDF from, defaults to stdin')
parser.add_argument('--floating-h', action='store_true',
                    help='Disable hydrogen layer')
args = parser.parse_args()

sdf = args.sdf.read()

inchi_args = []

if not args.floating_h:
    inchi_args.append('-FixedH')

with tempfile.TemporaryDirectory() as workdir:
    sdfpath = workdir + '/sdf'
    with open(sdfpath, 'w') as f:
        f.write(sdf)
    inchi = subprocess.Popen(['inchi-1', sdfpath, '/dev/stdout', 'NUL', 'NUL', '-AuxNone', '-Polymers', '-LargeMolecules', '-W300'] + inchi_args,
                             stdin=subprocess.PIPE,
                             encoding=sys.getdefaultencoding())
    inchi.communicate(sdf)
    inchi.wait()
    ret = inchi.returncode
    exit(inchi.returncode)
