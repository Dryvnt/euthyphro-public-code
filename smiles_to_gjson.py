#!/usr/bin/env python3

# convert a smiles string into gjson
# example usage: smiles_to_gjson.py "CN=C=O"
# example usage: echo CN=C=O | smiles_to_gjson.py


import argparse
import subprocess
import tempfile
import os
import sys

parser = argparse.ArgumentParser(
    description="Convert a SMILES string to GJSON format using MØD")
parser.add_argument(
    "smiles", nargs="?", help="Use given smiles string, otherwise read from stdin", action="store", type=str)
args = parser.parse_args()

smiles = args.smiles
if smiles is None:
    smiles = sys.stdin.read().strip()

sub_program_base = """import json

obj = {'edges': [], 'vertices': []}
""" \
f"smiles_string = '{smiles}'" \
    """
g = smiles(smiles_string)

for e in g.edges:
    l = int(e.bondType)
    s = e.source.id
    t = e.target.id
    obj['edges'].append({'label': l, 'source': s, 'target': t})
for v in g.vertices:
    expected_id = len(obj['vertices'])
    id = v.id
    assert(expected_id == id)
    l = v.stringLabel
    obj['vertices'].append({'label': l})
"""

with tempfile.TemporaryDirectory() as workdir:
    with tempfile.NamedTemporaryFile(dir=workdir) as workfile:
        with open(workdir + "/smiles.py", mode="w") as f:
            sub_print = f"print(json.dumps(obj), file=open('{workfile.name}', 'w'))\n"
            sub_program = sub_program_base + sub_print

            f.write(sub_program)

        # mod prints warnings to stdout, so we have to do weird
        # temporary file things instead of simple piping
        mod = subprocess.run(["mod", "-f", workdir + "/smiles.py", "--nopost", "-q"],
                             check=True,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE,
                             cwd=workdir,
                             encoding=sys.getdefaultencoding())
        print(mod.stdout, file=sys.stderr, end="")
        print(mod.stderr, file=sys.stderr, end="")
        workfile.seek(0)
        print(workfile.read().decode(sys.getdefaultencoding()), end="")
        exit(mod.returncode)
