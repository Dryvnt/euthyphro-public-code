#!/usr/bin/env python3

import time
import subprocess
import argparse
import sys
import csv

parser = argparse.ArgumentParser(
    description='Test exec on random FASTA of different length')
parser.add_argument('exec', type=str,
                    help='Executable to use')
parser.add_argument('nmin', type=int,
                    help='Starting size')
parser.add_argument('nmax', type=int,
                    help='End size')
parser.add_argument('nstep', type=int,
                    help='Stepsize from nmin to nmax')
parser.add_argument('--seed', type=int, nargs='?',
                    help='Optional seed (integer)')
parser.add_argument('--strip', action='store_true',
                    help='Preprocess with hydrogen stripping')
parser.add_argument('--collapse', action='store_true',
                    help='Preprocess with graph collapsing')

args = parser.parse_args()

nmin = args.nmin
nmax = args.nmax
nstep = args.nstep

seed = args.seed

fieldnames = ['n', 'fasta-overhead', 'smiles-overhead',
              'mod-overhead', 'preprocess-overhead', 'postprocess-overhead', 'permute-overhead', 'time', 'total']
writer = csv.DictWriter(sys.stdout, fieldnames=fieldnames)
writer.writeheader()

for n in range(nmin, nmax + 1, nstep):
    t = time.perf_counter()
    fasta_command = ['./random_fasta.py', str(n)]
    if seed is not None:
        fasta_command += ['-s', str(seed)]
    fasta = subprocess.run(fasta_command, check=True,
                           stdout=subprocess.PIPE, encoding=sys.getdefaultencoding()).stdout
    fasta_time = time.perf_counter() - t

    t = time.perf_counter()
    smiles = subprocess.run(['./fasta_to_smiles.py'], input=fasta, check=True,
                            stdout=subprocess.PIPE, encoding=sys.getdefaultencoding()).stdout
    smiles_time = time.perf_counter() - t

    t = time.perf_counter()
    gjson = subprocess.run(['./smiles_to_gjson.py'], input=smiles, check=True,
                           stderr=subprocess.DEVNULL,
                           stdout=subprocess.PIPE, encoding=sys.getdefaultencoding()).stdout
    mod_time = time.perf_counter() - t

    t = time.perf_counter()
    if args.strip:
        gjson = subprocess.run(['./strip_h.py'], input=gjson, check=True,
                               stdout=subprocess.PIPE, encoding=sys.getdefaultencoding()).stdout
    if args.collapse:
        gjson = subprocess.run(['./collapse.py'], input=gjson, check=True,
                               stdout=subprocess.PIPE, encoding=sys.getdefaultencoding()).stdout
    preprocess_time = time.perf_counter() - t

    t = time.perf_counter()
    gjson = subprocess.run(['./permute.py'], input=gjson, check=True,
                           stdout=subprocess.PIPE, encoding=sys.getdefaultencoding()).stdout
    permute_time = time.perf_counter() - t

    t = time.perf_counter()
    subprocess.run([args.exec, '-f', '-'], input=gjson, check=True,
                   stdout=subprocess.DEVNULL,
                   stderr=subprocess.DEVNULL,
                   encoding=sys.getdefaultencoding())
    elapsed = time.perf_counter() - t

    t = time.perf_counter()
    if args.collapse:
        gjson = subprocess.run(['./collapse.py', '-e'], input=gjson, check=True,
                               stdout=subprocess.PIPE, encoding=sys.getdefaultencoding()).stdout
    if args.strip:
        gjson = subprocess.run(['./strip_h.py', '-u'], input=gjson, check=True,
                               stdout=subprocess.PIPE, encoding=sys.getdefaultencoding()).stdout
    postprocess_time = time.perf_counter() - t

    total_time = preprocess_time + elapsed + postprocess_time
    print(f'{n} - {total_time}', file=sys.stderr)

    writer.writerow({
        'n': n,
        'fasta-overhead': fasta_time,
        'smiles-overhead': smiles_time,
        'mod-overhead': mod_time,
        'preprocess-overhead': preprocess_time,
        'postprocess-overhead': postprocess_time,
        'permute-overhead': permute_time,
        'time': elapsed,
        'total': total_time,
    })
