#!/usr/bin/env python3

import subprocess
import argparse
import sys
import os
import shutil
import itertools
import pathlib
import joblib

parser = argparse.ArgumentParser(
    description='Build euthyphro with a large selection of different options. Requires a build directory with makefile already generated.')
parser.add_argument('builddir', type=str,
                    help='Path to build directory')
args = parser.parse_args()

debug = [None, 'USE_LOG_DEBUG']
search_strategies = ['USE_SEARCH_DFS']
cell_selectors = ['USE_SELECTOR_F', 'USE_SELECTOR_FL']
size_2_refiner = [None, 'USE_REFINER_SIZE_2']
tree_refiners = [None, 'USE_REFINER_DEGREE_1', 'USE_REFINER_SUBTREE']
partial_leaf = [None]
cell_split = [None]
quotient = [None]

#partial_leaf = [None, 'USE_INVARIANT_PARTIAL_LEAF']
#cell_split = [None, 'USE_INVARIANT_CELL_SPLIT']
#quotient = [None, 'USE_INVARIANT_QUOTIENT']

options = [search_strategies, cell_selectors, size_2_refiner,
           tree_refiners, partial_leaf, cell_split, quotient, debug]

try:
    os.makedirs(args.builddir)
except:
    pass


def compile(choices):
    def get_flags(options, choice):
        flags = []
        for i, f in enumerate(options):
            if f is not None:
                toggle = 'ON' if choice == i else 'OFF'
                flags.append(f'-D{f}={toggle}')
        return flags

    def get_pretty_name(options, choice):
        f = options[choice]
        if f is None:
            return 'none'
        words = f.split('_')
        f = '_'.join(words[2:])
        return f.lower()

    flags = []
    for i in range(len(choices)):
        flags += get_flags(options[i], choices[i])

    exec_name = 'euthyphro'
    for i in range(len(choices)):
        exec_name += '-' + get_pretty_name(options[i], choices[i])

    subdir = args.builddir + '/' + exec_name + '.dir'
    try:
        os.makedirs(args.builddir)
    except:
        pass

    subprocess.run(['cmake', f'-B{subdir}', '-H.'] + flags, check=True)
    subprocess.run(['make', '--directory', subdir], check=True)
    shutil.copy2(f'{subdir}/euthyphro', f'{args.builddir}/{exec_name}')


options_range = list(map(list, map(range, map(len, options))))

joblib.Parallel(n_jobs=3, prefer='threads')(joblib.delayed(compile)(
    choices) for choices in itertools.product(*options_range))
