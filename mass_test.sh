#!/bin/sh

for file in $1/*; do
    echo $file
    ./test.py -f $file -n 5 || { echo 'FAIL ON' $file ; exit 1; }
done
