#!/usr/bin/env python3

import subprocess
import argparse
import sys

parser = argparse.ArgumentParser(
    description='Perform a quick test using first n characters of f, appended by a')
parser.add_argument('e', type=str,
                    help='Executable to use')
parser.add_argument('f', type=str,
                    help='File to read FASTA from')
parser.add_argument('n', type=int,
                    help='Number of characters to read from f')
parser.add_argument('a', type=str, nargs='*',
                    help='Characters to append to first n of f')
parser.add_argument('--strip', action='store_true',
                    help='Strip hydrogens')
parser.add_argument('--collapse', action='store_true',
                    help='Collapse graph')
parser.add_argument('--permute', action='store_true',
                    help='Permute graph before piping into executable')
args = parser.parse_args()
args.a = ''.join(args.a)

head = subprocess.run(['head',  '-c', f'{args.n}', f'{args.f}'], stdout=subprocess.PIPE, encoding=sys.getdefaultencoding(), check=True).stdout
head = head + args.a
smiles = subprocess.run(['./fasta_to_smiles.py'], input=head, check=True,
                        stdout=subprocess.PIPE, encoding=sys.getdefaultencoding()).stdout
gjson = subprocess.run(['./smiles_to_gjson.py'], input=smiles, check=True,
                       stdout=subprocess.PIPE, encoding=sys.getdefaultencoding()).stdout
if(args.strip):
    gjson = subprocess.run(['./strip_h.py'], input=gjson, check=True,
                           stdout=subprocess.PIPE, encoding=sys.getdefaultencoding()).stdout
if(args.collapse):
    gjson = subprocess.run(['./collapse.py'], input=gjson, check=True,
                           stdout=subprocess.PIPE, encoding=sys.getdefaultencoding()).stdout
if(args.permute):
    gjson = subprocess.run(['./permute.py'], input=gjson, check=True,
                           stdout=subprocess.PIPE, encoding=sys.getdefaultencoding()).stdout

gjson = subprocess.run([args.e, '-f', '-'], input=gjson, check=True,
                       stdout=subprocess.PIPE, encoding=sys.getdefaultencoding()).stdout

if(args.collapse):
    gjson = subprocess.run(['./collapse.py', '-e'], input=gjson, check=True,
                           stdout=subprocess.PIPE, encoding=sys.getdefaultencoding()).stdout
if(args.strip):
    gjson = subprocess.run(['./strip_h.py', '-u'], input=gjson, check=True,
                           stdout=subprocess.PIPE, encoding=sys.getdefaultencoding()).stdout
