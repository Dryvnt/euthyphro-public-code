#!/usr/bin/env python3

import json
import sys
import random
import copy
import argparse


def strip(obj):
    mapping = []
    i = 0
    for v in obj['vertices']:
        if v['label'] == 'H':
            mapping.append(None)
        else:
            mapping.append(i)
            i = i + 1
    vertices = [v for k, v in enumerate(
        obj['vertices']) if mapping[k] is not None]
    edges = []
    for e in obj['edges']:
        s = mapping[e['source']]
        t = mapping[e['target']]
        if s != None:
            if t != None:
                edges.append({'label': e['label'], 'source': s, 'target': t})
            else:  # t is a removed hydrogen
                vertices[s]['label'] = vertices[s]['label'] + 'H'

    obj['vertices'] = vertices
    obj['edges'] = edges


def unstrip(obj):
    num_verts = len(obj['vertices'])
    new_vertices = []
    new_edges = []
    for (v_idx, v) in enumerate(obj['vertices']):
        idx = len(v['label']) - 1
        num_h = 0
        while v['label'][idx] == 'H':
            idx = idx - 1
            num_h = num_h + 1
        v['label'] = v['label'][:len(v['label']) - num_h]
        for i in range(num_h):
            h_v = {'label': 'H'}
            h_e = {
                'source': v_idx,
                'target': num_verts + len(new_vertices),
                'label': 1
            }
            new_vertices.append(h_v)
            new_edges.append(h_e)
    obj['vertices'] += new_vertices
    obj['edges'] += new_edges


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Strip hydrogen atoms off of a molecule represented in GJSON.')
    parser.add_argument('-f', '--file', type=str, default='-',
                        help='File to read GJSON from. Set to - or leave undefined for stdin')
    parser.add_argument('-u', '--unstrip', action='store_true',
                        help='Perform unstripping instead of stripping')
    args = parser.parse_args()
    obj = None
    if(args.file == '-'):
        obj = json.loads(sys.stdin.read())
    else:
        with open(args.file, 'r') as fp:
            obj = json.loads(fp.read())
    if args.unstrip:
        unstrip(obj)
    else:
        strip(obj)
    print(json.dumps(obj))
