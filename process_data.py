#!/usr/bin/env python3

import json
import csv
import argparse
import sys

parser = argparse.ArgumentParser(
    description='Process hyperfine json data into csv for latex')
parser.add_argument('input', type=str,
                    help='Path to input json data file')
args = parser.parse_args()

with open(args.input, 'r') as infile:
    data = json.loads(infile.read())
    fieldnames = ['suffix', 'mean', 'stddev', 'min', 'max']
    outwriter = csv.DictWriter(sys.stdout, fieldnames)
    outwriter.writeheader()
    for entry in data['results']:
        command_words = entry['command'].split(' ')
        suffix = command_words[-1]
        if suffix == '':
            suffix = '-'
        row = {
            'suffix': suffix[-1:],
            'stddev': entry['stddev'],
            'mean': entry['mean'],
            'min': entry['min'],
            'max': entry['max']
        }
        outwriter.writerow(row)
