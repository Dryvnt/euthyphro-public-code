#include <cassert>
#include <cstdint>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <memory>
#include <random>
#include <string>
#include <tuple>
#include <typeinfo>

#include <graph_canon/aut/implicit_size_2.hpp>
#include <graph_canon/aut/pruner_basic.hpp>
#include <graph_canon/aut/pruner_schreier.hpp>
#include <graph_canon/canonicalization.hpp>
#include <graph_canon/gjson_graph_io.hpp>
#include <graph_canon/invariant/cell_split.hpp>
#include <graph_canon/invariant/partial_leaf.hpp>
#include <graph_canon/invariant/quotient.hpp>
#include <graph_canon/ordered_graph.hpp>
#include <graph_canon/refine/WL_1.hpp>
#include <graph_canon/refine/degree_1.hpp>
#include <graph_canon/refine/subtree.hpp>
#include <graph_canon/target_cell/f.hpp>
#include <graph_canon/target_cell/fl.hpp>
#include <graph_canon/target_cell/flm.hpp>
#include <graph_canon/target_cell/fs.hpp>
#include <graph_canon/tree_traversal/bfs-exp-m.hpp>
#include <graph_canon/tree_traversal/bfs-exp.hpp>
#include <graph_canon/tree_traversal/dfs-rand.hpp>
#include <graph_canon/tree_traversal/dfs.hpp>
#include <graph_canon/util.hpp>
#include <graph_canon/visitor/debug.hpp>
#include <graph_canon/visitor/stats.hpp>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graph_utility.hpp>
#include <boost/program_options.hpp>

#include "edge_handler_max_4.hpp"
#include "edge_handler_static_max.hpp"

#include "euthyphro_config.hpp"

namespace po = boost::program_options;

using GraphCanonSizeType = uint64_t;
using VertexLabelType = std::string;
using EdgeLabelType = int64_t;

using Graph = boost::adjacency_list<
    boost::vecS, boost::vecS, boost::undirectedS,
    boost::property<boost::vertex_name_t, VertexLabelType>,
    boost::property<boost::edge_name_t, EdgeLabelType>>;

// Alternatively: graph_canon::make_property_less(get(boost::vertex_name_t(), g)
struct vertex_label_comparator {
    const Graph& g;

    vertex_label_comparator(Graph& _g) : g(_g) {}

    //todo: get rid of templates, we as the user should know all types
    //helps for user-level description of how the library works
    //below doesn't work
    //using Key = boost::graph_traits<Graph>::vertex_descriptor;
    template <typename Key>
    bool operator()(const Key& lhs, const Key& rhs) const {
        const auto& lhs_label = get(boost::vertex_name_t(), g, lhs);
        const auto& rhs_label = get(boost::vertex_name_t(), g, rhs);
        return lhs_label < rhs_label;
    }
};

template <typename SizeType>
struct edge_label_comparator_impl
    : graph_canon::edge_handler_all_equal_impl<SizeType> {
    template <typename State, typename Edge>
    long long compare(State& state, const Edge& e_left,
                      const Edge& e_right) const {
        const long long l = get(boost::edge_name_t(), state.g, e_left);
        const long long r = get(boost::edge_name_t(), state.g, e_right);
        const long long out = l - r;
        return out;
    }
};

struct edge_label_comparator {
    template <typename SizeType>
    using type = edge_label_comparator_impl<SizeType>;

    template <typename SizeType>
    type<SizeType> make() {
        return {};
    }
};

int perform_canonicalization(std::istream& input, std::ostream& output) {
    const auto parHandler = []([[maybe_unused]] unsigned int src,
                               [[maybe_unused]] unsigned int tar) {
        return false;
    };
    const auto loopHandler = []([[maybe_unused]] unsigned int v) {
        return false;
    };

    Graph g;

    graph_canon::gjson::read(input, g, std::cerr, parHandler, loopHandler);

#ifdef USE_LOG_DEBUG
    std::ofstream debuglog;
    debuglog.open("debuglog.txt");
#endif

    auto stats = graph_canon::stats_visitor();

    // The order of visitors matters
    auto vis = make_visitor(
        graph_canon::refine_WL_1(),
#ifdef USE_REFINER_SIZE_2
        graph_canon::aut_implicit_size_2(), 
#endif
#ifdef USE_SEARCH_DFS
        graph_canon::traversal_dfs(),
#endif
#ifdef USE_SEARCH_BFS
        graph_canon::traversal_bfs_exp(),
#endif
#ifdef USE_SELECTOR_F
        graph_canon::target_cell_f(),
#endif
#ifdef USE_SELECTOR_FL
        graph_canon::target_cell_fl(),
#endif
#ifdef USE_SELECTOR_FLM
        graph_canon::target_cell_flm(),
#endif
#ifdef USE_REFINER_DEGREE_1
        graph_canon::refine_degree_1(),
#endif
#ifdef USE_REFINER_SUBTREE
        graph_canon::refine_subtree(),
#endif
        graph_canon::aut_pruner_basic(),
        //graph_canon::aut_pruner_schreier(),
#ifdef USE_INVARIANT_PARTIAL_LEAF
        graph_canon::invariant_partial_leaf(),
#endif
#ifdef USE_INVARIANT_CELL_SPLIT
        graph_canon::invariant_cell_split(),
#endif
#ifdef USE_INVARIANT_QUOTIENT
        graph_canon::invariant_quotient(),
#endif
#ifdef USE_LOG_DEBUG
        graph_canon::debug_visitor(false, false, true, false, true, std::cerr, &debuglog),
#endif
        stats);

    auto canon_res = graph_canon::canonicalize<int, false, false>(
        g, get(boost::vertex_index_t(), g), vertex_label_comparator(g),
        //edge_label_comparator(), vis);
        edge_handler_max_4(), vis);
        //edge_handler_static_max<5>(), vis);

    const auto& stats_data =
        get(graph_canon::stats_visitor::result_t(), std::get<1>(canon_res));
    std::cerr << stats_data;
    std::cerr << "subtree failed " << graph_canon::SUBTREE_NUM_FAIL << " times." << std::endl;

#ifdef USE_LOG_DEBUG
    debuglog << "]" << std::endl;
#endif

    const auto idx = std::get<0>(canon_res);
    auto idxMap = boost::make_iterator_property_map(
        idx.cbegin(), get(boost::vertex_index_t(), g));
    using OrderedGraph = graph_canon::ordered_graph<Graph, decltype(idxMap)>;
    OrderedGraph g_ordered(
        g, idxMap,
        graph_canon::make_property_less(get(boost::edge_name_t(), g)));

    auto vert_func = [](auto& g, auto v) {
        return get(boost::vertex_name_t(), g.data.g, v);
    };
    auto edge_func = [](auto& g, auto e) {
        return get(boost::edge_name_t(), g.data.g, e);
    };
    auto vert_map_func = [&idxMap](auto v) { return get(idxMap, v); };

    graph_canon::gjson::write(output, g_ordered, vert_func, edge_func,
                              vert_map_func);

    return EXIT_SUCCESS;
}

int main(int argc, char* argv[]) {
    std::string file;

    po::options_description options_desc("Options");
    options_desc.add_options()
        // Help
        ("help,h", "Print help message.")
        // Version
        ("version,v", "Prints version.")
        // Input file
        ("input-file,f", po::value<std::string>(&file)->required(),
         "File with graph in GJSON format. Use '-' to read from stdin.");

    po::variables_map vm;
    try {
        po::store(
            po::command_line_parser(argc, argv).options(options_desc).run(),
            vm);
    } catch (po::unknown_option& e) {
        std::cerr << "Unrecognized option '-j'";
        std::cerr << " See 'euthyphro --help'." << std::endl;
        return EXIT_FAILURE;
    }

    if (vm.count("help") || vm.size() == 0) {
        std::cout << "usage: euthyphro [Options]" << std::endl;
        std::cout << options_desc;
        return EXIT_SUCCESS;
    }

    if (vm.count("version")) {
        std::cout << "euthyphro version ";
        std::cout << EUTHYPHRO_VERSION_MAJOR;
        std::cout << ".";
        std::cout << EUTHYPHRO_VERSION_MINOR;
        std::cout << " (";
        std::cout << EUTHYPHRO_VERSION_BRANCH;
        std::cout << "-";
        std::cout << EUTHYPHRO_VERSION_COMMIT;
        std::cout << ")";
        std::cout << std::endl;
        return EXIT_SUCCESS;
    }

    try {
        po::notify(vm);
    } catch (po::required_option& e) {
        std::cerr << "The option '" << e.get_option_name()
                  << "' is required but missing.";
        std::cerr << " See 'euthyphro --help'." << std::endl;
        return EXIT_FAILURE;
    }

    if (file == "-") {
        return perform_canonicalization(std::cin, std::cout);
    } else {
        // TODO: File not found?
        std::ifstream fs(file);
        assert(fs.good());
        return perform_canonicalization(fs, std::cout);
    }
}
