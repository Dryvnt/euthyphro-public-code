#ifndef EUTHYPHRO_EDGE_HANDLER_MAX_4_HPP
#define EUTHYPHRO_EDGE_HANDLER_MAX_4_HPP

#include <vector>

template <typename SizeType>
struct edge_handler_max_4_impl {
    using Counter = std::array<SizeType, 4>;

    struct CellData {
        CellData() {
            hit_count.fill(0);
            acc_count.fill(0);
        }

      public:
        // For non-singleton
        Counter hit_count;
        // For singleton
        // accumulated count from above, i.e.:
        // - [3] = #type 4 bonds
        // - [2] = [3] + #type 3 bonds
        // - [1] = [2] = #type 2 bonds
        // - [0] = [1] = #type 1 bonds
        Counter acc_count;
    };
    static constexpr SizeType Max = 256;

    template <typename State>
    void initialize(const State& state) {
        Counter c;
        c.fill(0);
        counters.resize(state.n, c);
        cellData.resize(state.n);
    }

    //========================================================================
    // Generic Refiner
    //========================================================================

    template <typename State, typename Node, typename Edge>
    void add_edge(State& state, [[maybe_unused]] Node& node,
                  const SizeType cell, [[maybe_unused]] const SizeType cell_end,
                  const Edge& e_out) {
        SizeType i = get(boost::edge_name_t(), state.g, e_out);
        assert(i != 0); // invalid bond, should not happen
        --i;
        assert(i < 4);
        const auto v_out = target(e_out, state.g);
        const auto v_out_idx = state.idx[v_out];
        ++counters[v_out_idx][i];
        ++cellData[cell].hit_count[i];
    }

    template <bool ParallelEdges, bool Loops, typename Partition,
              typename Splits>
    void sort_equal_hit(Partition& pi, const SizeType cell,
                        const SizeType cell_end, const SizeType max,
                        Splits& splits) {
        sort_range<ParallelEdges, Loops>(pi, cell, cell, cell_end, max, splits);
    }

    template <bool ParallelEdges, bool Loops, typename Partition,
              typename Splits>
    void sort_partitioned(Partition& pi, const SizeType cell,
                          const SizeType cell_mid, const SizeType cell_end,
                          const SizeType max_count, Splits& splits) {
        sort_range<ParallelEdges, Loops>(pi, cell, cell_mid, cell_end,
                                         max_count, splits);
    }

    template <bool ParallelEdges, bool Loops, typename Partition,
              typename Splits>
    void sort_duplicate_partitioned_equal_hit(
        Partition& pi, const SizeType cell, const SizeType cell_mid,
        const SizeType cell_end, const SizeType max, Splits& splits) {
        sort_range<ParallelEdges, Loops>(pi, cell, cell_mid, cell_end, max,
                                         splits);
    }

    template <bool ParallelEdges, bool Loops, typename Partition,
              typename Splits, typename Counters>
    void sort_duplicate_partitioned(Partition& pi, const SizeType cell,
                                    const SizeType cell_mid,
                                    const SizeType cell_end, const SizeType max,
                                    [[maybe_unused]] const SizeType max_count,
                                    [[maybe_unused]] const Counters& counters,
                                    Splits& splits) {
        sort_range<ParallelEdges, Loops>(pi, cell, cell_mid, cell_end, max,
                                         splits);
    }

    template <bool ParallelEdges, bool Loops, typename Partition,
              typename Splits, typename Counters>
    void sort(Partition& pi, const SizeType cell, const SizeType cell_end,
              const SizeType max, [[maybe_unused]] const SizeType max_count,
              SizeType& first_non_zero,
              [[maybe_unused]] const Counters& counters, Splits& splits) {
        assert(max_count != cell_end - cell); // handled by refiner
        assert(max > 1);                      // handled by refiner
        first_non_zero = sort_range<ParallelEdges, Loops>(
            pi, cell, cell, cell_end, max, splits);
    }

  private:
    template <typename Partition, typename Splits, typename First,
              typename Last>
    void sort_level(Partition& pi, const SizeType cell,
                    const SizeType idx_first, Splits& splits, const First first,
                    const Last last, const SizeType level) {
        if (level == 4)
            return;
        if (cellData[cell].hit_count[level] == 0) {
            return sort_level(pi, cell, idx_first, splits, first, last,
                              level + 1);
        }
        std::array<SizeType, Max> ends;
        sorter(first, last,
               [this, level](const SizeType i) { return counters[i][level]; },
               [&ends](const auto& end) {
                   std::copy(end.begin(), end.end(), ends.begin());
               },
               [&pi](auto iter, const auto value) {
                   pi.put_element_on_index(value, iter - pi.begin());
               });
        const auto size = last - first;
        auto prev = 0;
        for (auto end_idx = 0; ends[end_idx] != size;
             prev = ends[end_idx], ++end_idx) {
            const auto end = ends[end_idx];
            if (end == prev)
                continue;
            const auto sub_first = first + prev;
            const auto sub_last = first + end;
            sort_level(pi, cell, idx_first + prev, splits, sub_first, sub_last,
                       level + 1);
            splits.push_back(end + idx_first);
        }
        // and the last bucket, where we don't append an end split
        const auto sub_first = first + prev;
        const auto sub_last = first + size;
        assert(prev != size);
        sort_level(pi, cell, idx_first + prev, splits, sub_first, sub_last,
                   level + 1);
    }

    template <bool ParallelEdges, bool Loops, typename Partition,
              typename Splits>
    SizeType sort_range(Partition& pi, const SizeType cell,
                        const SizeType idx_first, const SizeType idx_last,
                        const SizeType max, Splits& splits) {
        const auto first = pi.begin() + idx_first;
        const auto last = pi.begin() + idx_last;
        // use counting sort to sort for low max_count
        if (max < Max) {
            const auto pre_sort_num_splits = splits.size();
            sort_level(pi, cell, idx_first, splits, first, last, 0);
            const auto& count = counters[pi.get(idx_first)];
            const auto all_zero =
                std::all_of(count.begin(), count.end(),
                            [](const auto c) { return c == 0; });
            if (all_zero)
                return splits[pre_sort_num_splits];
            else
                return idx_first;
        }
        // fallback, just sort
        std::sort(first, last, [this](const auto a, const auto b) {
            return counters[a] < counters[b];
        });
        pi.reset_inverse(idx_first, idx_last);
        // Scan the refinee invariants and split it if needed, save new subset beginnings.
        for (SizeType i_refinee = idx_first + 1; i_refinee < idx_last;
             i_refinee++) {
            const SizeType refinee_prev_idx = pi.get(i_refinee - 1);
            const SizeType refinee_idx = pi.get(i_refinee);
            if (counters[refinee_prev_idx] < counters[refinee_idx]) {
                assert(i_refinee != idx_first);
                assert(i_refinee != idx_last);
                if (!splits.empty())
                    assert(i_refinee != splits.back());
                splits.push_back(i_refinee);
            }
        }
        return idx_first; // TODO: we could check this
    }

  public:
    template <typename State, typename Node>
    void clear_cell([[maybe_unused]] State& state, Node& node,
                    const SizeType cell, const SizeType cell_end) {
        cellData[cell].hit_count.fill(0);
        for (auto i = cell; i != cell_end; ++i)
            counters[node.pi.get(i)].fill(0);
    }

    template <typename State, typename Node>
    void clear_cell_aborted(State& state, Node& node, const SizeType cell,
                            const SizeType cell_end) {
        clear_cell(state, node, cell, cell_end);
    }

    //========================================================================
    // Singleton Refiner
    //========================================================================

    template <typename State, typename Node, typename Edge>
    void
    add_edge_singleton_refiner(State& state, Node& node, const SizeType cell,
                               const SizeType cell_end, const Edge& e_out,
                               [[maybe_unused]] const SizeType target_pos) {
        SizeType i = get(boost::edge_name_t(), state.g, e_out);
        assert(i != 0); // invalid bond, should not happen
        --i;
        assert(i < 4);

        // we want to keep the partitioning into the four groups of bond types
        // X00...0011...1122...2233...33
        auto& acc_count = cellData[cell].acc_count;
        // first increase the span
        switch (i) {
        case 3:
            ++acc_count[3];
            [[fallthrough]];
        case 2:
            ++acc_count[2];
            [[fallthrough]];
        case 1:
            ++acc_count[1];
            [[fallthrough]];
        case 0:
            ++acc_count[0];
        }

        // and then do the swapping down
        switch (i) {
        case 1: // 000...0X11...1122...2233...33
            // but if there are no 0's, we are done
            if (acc_count[0] != acc_count[1]) {
                const auto target_0 = cell_end - acc_count[0];
                const auto target_1 = cell_end - acc_count[1];
                const auto elem_0 = node.pi.get(target_0);
                const auto elem_1 = node.pi.get(target_1);
                node.pi.put_element_on_index(elem_1, target_0);
                node.pi.put_element_on_index(elem_0, target_1);
            }
            break;
        case 2: // 000...0111...1X22...2233...33
        {
            const auto target_0 = cell_end - acc_count[0];
            const auto target_1 = cell_end - acc_count[1];
            const auto target_2 = cell_end - acc_count[2];
            const auto elem_0 = node.pi.get(target_0);
            if (acc_count[0] != acc_count[1]) {
                const auto elem_1 = node.pi.get(target_1);
                node.pi.put_element_on_index(elem_1, target_0);
            }
            if (acc_count[1] != acc_count[2]) {
                const auto elem_2 = node.pi.get(target_2);
                node.pi.put_element_on_index(elem_2, target_1);
            }
            if (acc_count[0] != acc_count[1] || acc_count[1] != acc_count[2])
                node.pi.put_element_on_index(elem_0, target_2);
        } break;
        case 3: // 000...0111...1222...2X33...33
        {
            const auto target_0 = cell_end - acc_count[0];
            const auto target_1 = cell_end - acc_count[1];
            const auto target_2 = cell_end - acc_count[2];
            const auto target_3 = cell_end - acc_count[3];
            const auto elem_0 = node.pi.get(target_0);
            if (acc_count[0] != acc_count[1]) {
                const auto elem_1 = node.pi.get(target_1);
                node.pi.put_element_on_index(elem_1, target_0);
            }
            if (acc_count[1] != acc_count[2]) {
                const auto elem_2 = node.pi.get(target_2);
                node.pi.put_element_on_index(elem_2, target_1);
            }
            if (acc_count[2] != acc_count[3]) {
                const auto elem_3 = node.pi.get(target_3);
                node.pi.put_element_on_index(elem_3, target_2);
            }
            if (acc_count[0] != acc_count[1] || acc_count[1] != acc_count[2]
                || acc_count[2] != acc_count[3])
                node.pi.put_element_on_index(elem_0, target_3);
        } break;
        }
    }

    template <bool ParallelEdges, bool Loops, typename Partition,
              typename Splits>
    void sort_singleton_refiner([[maybe_unused]] Partition& pi,
                                const SizeType cell,
                                [[maybe_unused]] const SizeType cell_mid,
                                const SizeType cell_end, Splits& splits) {
        const auto& acc_count = cellData[cell].acc_count;
        const auto target_0 = cell_end - acc_count[0];
        const auto target_1 = cell_end - acc_count[1];
        const auto target_2 = cell_end - acc_count[2];
        const auto target_3 = cell_end - acc_count[3];
        assert(target_0 == cell_mid);
        if (target_1 != target_0 && target_1 != cell_end)
            splits.push_back(target_1);
        if (target_2 != target_1 && target_2 != cell_end)
            splits.push_back(target_2);
        if (target_3 != target_2 && target_3 != cell_end)
            splits.push_back(target_3);
    }

    template <typename State, typename Node>
    void clear_cell_singleton_refiner(
        [[maybe_unused]] State& state, [[maybe_unused]] Node& node,
        const SizeType cell, [[maybe_unused]] const SizeType cell_end) {
        cellData[cell].acc_count.fill(0);
    }

    template <typename State, typename Node>
    void clear_cell_singleton_refiner_aborted(State& state, Node& node,
                                              const SizeType cell,
                                              const SizeType cell_end) {
        clear_cell_singleton_refiner(state, node, cell, cell_end);
    }

  private:
    graph_canon::counting_sorter<SizeType, Max> sorter;

  public:
    template <typename State, typename Edge>
    long long compare(State& state, const Edge e_left,
                      const Edge e_right) const {
        const long long l = get(boost::edge_name_t(), state.g, e_left);
        const long long r = get(boost::edge_name_t(), state.g, e_right);
        const long long out = l - r;
        return out;
    }

  public:
    std::vector<Counter> counters;
    std::vector<CellData> cellData;
};

struct edge_handler_max_4 {
    template <typename SizeType>
    using type = edge_handler_max_4_impl<SizeType>;

    template <typename SizeType>
    auto make() const {
        return edge_handler_max_4_impl<SizeType>();
    }
};

#endif /* EUTHYPHRO_EDGE_HANDLER_MAX_4_HPP */
