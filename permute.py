#!/usr/bin/env python3

import json
import sys
import random
import copy
import argparse


def permute(obj):
    idx = list(range(len(obj['vertices'])))
    random.shuffle(idx)

    inv_idx = [None] * len(idx)
    for i in range(len(idx)):
        inv_idx[idx[i]] = i

    verts = copy.deepcopy(obj['vertices'])
    for i in range(len(verts)):
        obj['vertices'][i] = verts[idx[i]]
    for i in range(len(obj['edges'])):
        src = obj['edges'][i]['source']
        tar = obj['edges'][i]['target']
        obj['edges'][i]['source'] = inv_idx[src]
        obj['edges'][i]['target'] = inv_idx[tar]
    random.shuffle(obj['edges'])


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Permute a graph in GJSON format.')
    parser.add_argument('-f', '--file', type=str, default='-',
                        help='File to read GJSON from. Set to - or leave undefined for stdin')
    args = parser.parse_args()
    obj = None
    if(args.file == '-'):
        obj = json.loads(sys.stdin.read())
    else:
        with open(args.file, 'r') as fp:
            obj = json.loads(fp.read())
    permute(obj)
    print(json.dumps(obj))
