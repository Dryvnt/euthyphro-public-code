#!/usr/bin/env python3

import argparse
import subprocess
import tempfile
import os
import sys

parser = argparse.ArgumentParser(
    description='Convert a fasta protein string to SMILES string')
parser.add_argument('-f', '--file', type=str, default='-',
                    help='File to read from. set to - or leave undefined for stdin')
parser.add_argument('--circular', action='store_true',
                    help='If set, output protein will be circular')
args = parser.parse_args()

fasta_input = None
if(args.file == '-'):
    fasta_input = sys.stdin.read()
else:
    with open(args.file, 'r') as fp:
        fasta_input = fp.read()

fasta_lines = [line for line in fasta_input.split('\n') if len(
    line) > 0 and not (line.startswith('>') or line.startswith(';'))]

fasta = ''.join(fasta_lines).replace(' ', '')

# DOES NOT SUPPORT STEREOCHEMISTRY
# basic pattern: NC(...)C(=O), sometimes broken?
# end chain with O
# replace '#1', '#2', ... '#n' with an unique integers

amino_acids = {
    'A': 'NC(C)C(=O)',
    #'B': Aspartic acid or Asparagine ?
    'C': 'NC(CS)C(=O)',
    'D': 'NC(CC(=O)O)C(=O)',
    'E': 'NC(CCC(=O)O)C(=O)',
    'F': 'N[C@@H](CC1=CC=CC=C1)C(=O)',
    'G': 'NC()C(=O)',
    'H': 'N[C@@H](CC1=CNC=N1)C(=O)',
    'I': 'N[C@@H]([C@H](CC)C)C(=O)',
    #'J': Leucine or Isoleucine ?
    'K': 'NC(CCCCN)C(=O)',
    'L': 'N[C@@H](CC(C)C)C(=O)',
    'M': 'NC(CCSC)C(=O)',
    'N': 'N[C@@H](CC(=O)N)C(=O)',
    #'O': 'pyrrolysine', not used
    'P': 'N1CCCC1C(=O)',
    'Q': 'NC(CCC(=O)N)C(=O)',
    'R': 'NC(CCCNC(=N)N)C(=O)',
    'S': 'N[C@@H](CO)C(=O)',
    'T': 'N[C@@H]([C@H](O)C)C(=O)',
    #'U': 'selonocysteine', not used
    'V': 'N[C@@H](C(C)C)C(=O)',
    'W': 'N[C@@H](cc1c2ccccc2nc1)C(=O)',
    'Y': 'N[C@@H](Cc1ccc(O)cc1)C(=O)',
    #'Z': Glutamic acid or Glutamine ?
    #'X': any lmao
}

out = ''
for acid in fasta:
    part = amino_acids[acid]
    out += part

if args.circular:
    assert(out[-4:] == '(=O)')
    out = out[:1] + '3' + out[1:-4] + '3(=O)'
else:
    out += 'O'
print(out)
