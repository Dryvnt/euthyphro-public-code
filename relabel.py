#!/usr/bin/env python3

import json
import sys
import random
import argparse

# changes obj in place


def relabel(obj, n_v, n_e):
    if(n_v > 0):
        for v in obj['vertices']:
            v['label'] = random.randrange(n_v)
    if(n_e > 0):
        for e in obj['edges']:
            e['label'] = random.randrange(n_e)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Relabel a graph in GJSON format.')
    parser.add_argument('-f', '--file', type=str, default='-',
                        help='File to read GJSON from. Set to - or leave undefined for stdin')
    parser.add_argument('-v', '--vlabels', type=int, default=0,
                        help='Number of labels to use for vertices. Will not relabel if 0.')
    parser.add_argument('-e', '--elabels', type=int, default=0,
                        help='Number of labels to use for edges. Will not relabel if 0.')
    args = parser.parse_args()
    obj = None
    if(args.file == '-'):
        obj = json.loads(sys.stdin.read())
    else:
        with open(args.file, 'r') as fp:
            obj = json.loads(fp.read())
    relabel(obj, args.vlabels, args.elabels)
    print(json.dumps(obj))
