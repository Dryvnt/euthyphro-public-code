This is the code that I wrote for my master thesis on Graph Canonicalization and Algorithm Engineering. I provide the code on this repository as an example of my work.

The program this code implements, Euthyphro, canonicalizes graphs. It relies on an old unreleased version of [GraphCanon](https://github.com/jakobandersen/graph_canon) that I extended for the thesis. This unreleased version remains unreleased, but the files I added can be found in the `other` folder.

Because the original git repository contained more than just code (e.g. notes w/ test data), I have stripped this repo of all history and these unrelated files.

I would be more than happy to explain anything about the code or project, and if you see me in person I will happily give a live demo if such is desired.
