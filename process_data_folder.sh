#!/bin/bash

INDIR=$1
OUTDIR=$2

mkdir -p $OUTDIR
mkdir -p $OUTDIR/scale

for f in $INDIR/*.json
do
    bigname=$(basename -- "$f")
    smallname="${bigname%.*}"
    ./process_data.py $f > "${OUTDIR}"/"${smallname}".csv
done


for f in $INDIR/scale/*.csv
do
    bigname=$(basename -- "$f")
    cp $f ${OUTDIR}/scale/${bigname}
done
