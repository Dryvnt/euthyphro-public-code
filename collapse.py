#!/usr/bin/env python3

import json
import argparse
import sys
import functools
import copy

MAGIC_EXPANDME_BEGIN = '['
MAGIC_EXPANDME_END = ']'
MAGIC_PREFIX_IMCLEAN = '#'


def collapse(graph):
    vertex_edge_map = {}
    degree = {}
    for i, e in enumerate(graph['edges']):
        s = e['source']
        t = e['target']
        degree[s] = degree.get(s, 0) + 1
        degree[t] = degree.get(t, 0) + 1
        vertex_edge_map[s] = {'parent_idx': t,
                              'edge_idx': i, 'edge_label': e['label']}
        vertex_edge_map[t] = {'parent_idx': s,
                              'edge_idx': i, 'edge_label': e['label']}

    child_candidates = [i for i, _ in enumerate(
        graph['vertices']) if degree[i] == 1]
    parent_candidates = [i for i, _ in enumerate(
        graph['vertices']) if degree[i] > 1]

    def child_cmp(a, b):
        a_label = graph['vertices'][a]['label']
        b_label = graph['vertices'][b]['label']
        if a_label < b_label:
            return -1
        if b_label < a_label:
            return 1

        assert(degree[a] == degree[b] == 1)
        a_elabel = vertex_edge_map[a]['edge_label']
        b_elabel = vertex_edge_map[b]['edge_label']
        if a_elabel < b_elabel:
            return -1
        if b_elabel < a_elabel:
            return 1
        return 0

    child_candidates.sort(key=functools.cmp_to_key(child_cmp))

    def get_id_tuple(i):
        i_vl = graph['vertices'][i]['label']
        i_el = vertex_edge_map[i]['edge_label']
        return (i_vl, i_el)

    collapsed = {}
    collapse_children = {}
    for i in child_candidates:
        vem = vertex_edge_map[i]
        p_i = vem['parent_idx']
        if p_i not in parent_candidates:
            continue
        id_tuple = get_id_tuple(i)
        collapse_children[p_i] = collapse_children.get(p_i, {})
        collapse_children[p_i][id_tuple] = collapse_children[p_i].get(
            id_tuple, 0) + 1
        collapsed[i] = True

    if len(collapsed) == 0:
        return None

    new_vertices = []
    old_to_new_map = {}
    new_to_old_map = {}
    for i, v in enumerate(graph['vertices']):
        if i in collapsed:
            continue
        children = collapse_children.get(i, {})
        new_label = None
        if len(children) == 0:
            new_label = MAGIC_PREFIX_IMCLEAN + v['label']
        else:
            c_keys = sorted(children.keys())
            c_serial = []
            c_info = ''
            for vl, el in c_keys:
                num = children[(vl, el)]
                c_info += f'{num},{MAGIC_EXPANDME_BEGIN}{vl}{MAGIC_EXPANDME_END},{el},'

            new_label = MAGIC_EXPANDME_BEGIN + \
                c_info + MAGIC_EXPANDME_END + v['label']

        new_vertices.append({'label': new_label})
        new_idx = len(new_vertices) - 1
        old_to_new_map[i] = new_idx
        new_to_old_map[new_idx] = i

    new_edges = []
    for i, e in enumerate(graph['edges']):
        s = e['source']
        t = e['target']
        if s in collapsed or t in collapsed:
            continue
        assert(s in old_to_new_map)
        assert(t in old_to_new_map)
        new_edge = {
            'source': old_to_new_map[s],
            'target': old_to_new_map[t],
            'label': e['label'],
        }
        new_edges.append(new_edge)

    new_graph = {
        'edges': new_edges,
        'vertices': new_vertices,
    }

    return new_graph


def get_matching_end(l, begin_idx):
    depth = 0
    for i, c in enumerate(l[begin_idx:]):
        if c == MAGIC_EXPANDME_BEGIN:
            depth += 1
        if c == MAGIC_EXPANDME_END:
            depth -= 1
            if depth == 0:
                return i + begin_idx


def parse_label(label):
    end_main = get_matching_end(label, 0)
    prefix = label[1:end_main]
    orig_label = label[end_main + 1:]
    children = []
    to_parse = prefix
    while to_parse:
        num_begin = 0
        num_end = to_parse.index(',', num_begin)
        sublabel_begin = num_end + 1
        sublabel_end = get_matching_end(to_parse, sublabel_begin)
        elabel_begin = sublabel_end+2
        elabel_end = to_parse.index(',', elabel_begin)

        num = int(to_parse[num_begin:num_end])
        sublabel = to_parse[sublabel_begin+1:sublabel_end]
        elabel = to_parse[elabel_begin:elabel_end]
        to_parse = to_parse[elabel_end+1:]
        for _ in range(num):
            children.append({'vl': sublabel, 'el': elabel})
    return {
        'label': orig_label,
        'children': children
    }


def expand(graph):
    expandable = True
    # all vertices must be expandable
    for v in graph['vertices']:
        expandme = v['label'].startswith(MAGIC_EXPANDME_BEGIN)
        imclean = v['label'].startswith(MAGIC_PREFIX_IMCLEAN)
        if not (imclean or expandme):
            expandable = False

    if not expandable:
        return None

    num_vertices = len(graph['vertices'])
    new_vertices = []
    new_vertices_expanded = []
    new_edges_expanded = []

    for i, v in enumerate(graph['vertices']):
        expandme = v['label'].startswith(MAGIC_EXPANDME_BEGIN)
        imclean = v['label'].startswith(MAGIC_PREFIX_IMCLEAN)
        if expandme:

            l_info = parse_label(v['label'])

            new_vertices.append({'label': l_info['label']})
            for c in l_info['children']:
                new_idx = num_vertices + len(new_vertices_expanded)
                new_vertices_expanded.append({'label': c['vl']})
                new_edges_expanded.append({
                    'label': c['el'],
                    'source': i,
                    'target': new_idx
                })
        if imclean:
            label = v['label'][1:]
            new_vertices.append({'label': label})

    new_graph = {
        'edges': graph['edges'] + new_edges_expanded,
        'vertices': new_vertices + new_vertices_expanded
    }

    return new_graph


class ExpandFlagAction(argparse.Action):
    def __call__(self, parser, ns, values, option):
        val = option == '--collapse'
        setattr(ns, self.dest, val)


parser = argparse.ArgumentParser(
    description='Collapse degree 1 graph structures'
)
parser.add_argument('infile', nargs='?', type=argparse.FileType(
    'r'), default=sys.stdin, help='Input file. Defaults to stdin if no arg given')
parser.add_argument('outfile', nargs='?', type=argparse.FileType(
    'w'), default=sys.stdout, help='Output file. Defaults to stdout if no arg given')
parser.add_argument('--collapse', action='store_const', default=True,
                    const=True, dest='collapse', help='Collapse input graph. Default action')
parser.add_argument('--expand', '-e', action='store_const',
                    const=False, dest='collapse', help='Expand input graph')
args = parser.parse_args()

action = collapse if args.collapse else expand
action_string = 'Collapse' if args.collapse else 'Expand'

graph = json.loads(args.infile.read())
i = 0
while True:
    print(f'{action_string} round {i}', file=sys.stderr)
    i += 1
    new_graph = action(graph)
    if new_graph == None:
        break
    graph = new_graph
args.outfile.write(json.dumps(graph, sort_keys=True))
args.outfile.write('\n')
