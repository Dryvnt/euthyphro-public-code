#!/usr/bin/env python3

# convert a smiles string into sdf
# example usage: smiles_to_sdf.py 'CN=C=O'
# example usage: echo CN=C=O | smiles_to_sdf.py


import argparse
import subprocess
import tempfile
import os
import sys

parser = argparse.ArgumentParser(
    description='Convert a SMILES string to SDF format using OpenBabel')
parser.add_argument(
    'smiles', nargs='?', help='Use given smiles string, otherwise read from stdin', action='store', type=str)
args = parser.parse_args()

smilepath = '/dev/stdin' if args.smiles is None else os.path.realpath(
    args.smiles)

smiles = ''
with open(smilepath, 'r') as smilefile:
    smiles = smilefile.read()

with tempfile.TemporaryDirectory() as workdir:
    babel = subprocess.Popen(['babel', '-ismi', '/dev/stdin', '-osdf', '/dev/stdout'],
                             stdin=subprocess.PIPE,
                             cwd=workdir,
                             encoding=sys.getdefaultencoding())
    babel.communicate(smiles)
    babel.wait()
    exit(babel.returncode)
